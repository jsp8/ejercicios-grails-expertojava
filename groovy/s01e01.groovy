class Todo {
    String titulo;
    String descripcion;
}

Todo todo1 = new Todo(titulo: "Lavadora", descripcion:"Poner lavadora")
Todo todo2 = new Todo(titulo: "Impresora", descripcion:"Comprar cartuchos impresora")
Todo todo3 = new Todo(titulo: "Películas", descripcion:"Devolver películas")
todos = [todo1, todo2, todo3]

todos.each { todo ->
    println todo.getTitulo() + ", " + todo.getDescripcion()
}
return