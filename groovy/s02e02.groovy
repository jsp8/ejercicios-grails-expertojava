class Calculadora {
    String operador
    int op1
    int op2
  
    String resultado() {
        switch (operador)
        {
            case "+": return op1 + op2
            case "-": return op1 - op2
            case "*": return op1 * op2
            case "/": assert op2 > 0:"División por cero"; return op1 / op2
            default: return "Operador no encontrado"
        }
    }
}

System.in.withReader {

        def operador
        print 'Introduzca operador (+, -, *, /) (q para finalizar): '
        operador = it.readLine()
    
        while (operador != "q") {
            print 'Introduzca operando1: '
            def op1 = Integer.valueOf(it.readLine())
            print 'Introduzca operando2: '
            def op2 = Integer.valueOf(it.readLine())
            
            Calculadora calculadora = new Calculadora(operador: operador, op1: op1, op2: op2)
            println "Resultado: " + calculadora.resultado()
            
            print 'Introduzca operador (+, -, *, /) (q: para finalizar): '
            operador = it.readLine()
        }
}