/* Añade aquí la implementación del factorial en un closure */

def factorial = {
    
    resultado = 1
    
    for(i = 1; i <= it; i++)
        resultado *= i
    
    return resultado
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

println "------- Factorial -------" 

lista = (1..5).toList()
for(i in lista)
    println "(" + i + "): " + factorial(i)
    
println " "    

println "------- Fechas -------"
ayer = { it - 1 }
mañana = { it + 1 }
 
def fechas = []
fecha_inicial = new Date()
fecha_final = fecha_inicial + 6;
fechas = (fecha_inicial..fecha_final).toList()

for(i in fechas)
    println "Hoy: " + i + ", ayer: " + ayer(i) + ", mañana: " + mañana(i)
    