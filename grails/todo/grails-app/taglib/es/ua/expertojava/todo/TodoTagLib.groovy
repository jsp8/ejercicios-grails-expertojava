package es.ua.expertojava.todo

class TodoTagLib {
    //static defaultEncodeAs = [taglib:'html']
    static encodeAsForTags = [printIconFromBoolean: [taglib:'none']]

    static namespace = "todo"

    def printIconFromBoolean = { attrs ->

        boolean valor = attrs['value']

        if (valor == true)
            out << asset.image(src: "ok.png")
        else
            out << asset.image(src: "cancel.png")
    }
}
