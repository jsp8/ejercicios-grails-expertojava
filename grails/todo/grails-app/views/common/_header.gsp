<div id="header">
    <div id="menu">
        <nobr>
            <sec:ifLoggedIn>
                <strong><sec:username/></strong>
                <g:form controller="logout" method="POST">
                    <g:actionSubmit value="Logout" action="index"></g:actionSubmit>
                </g:form>
            </sec:ifLoggedIn>

            <sec:ifNotLoggedIn>
                <g:link controller="login" action="auth">Login</g:link>
            </sec:ifNotLoggedIn>
        </nobr>
    </div>
</div>
