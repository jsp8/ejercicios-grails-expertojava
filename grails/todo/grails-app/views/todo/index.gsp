
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>

		<h1><a href="http://grails.org/Searchable+Plugin" target="_blank">Grails Searchable Plugin</a></h1>
        <form action="/todo/searchable" method="get" id="searchableForm" name="searchableForm" >
			<input type="text" name="q" value="pastel" size="50" id="q" /> <input type="submit" value="Search" />
        </form>

		<div id="list-todo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<th>Opciones</th>
						<g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />
						<g:sortableColumn property="date" title="${message(code: 'todo.date.label', default: 'Date')}" />
						<g:sortableColumn property="dateDone" title="${message(code: 'todo.dateDone.label', default: 'DateDone')}" />
						<g:sortableColumn property="url" title="${message(code: 'todo.url.label', default: 'Url')}" />
						<th><g:message code="todo.category.label" default="Category" /></th>
						<th><g:message code="todo.done.label" default="Done" /></th>

					</tr>
				</thead>
				<tbody>
				<g:each in="${todoInstanceList}" status="i" var="todoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td>
							<g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
								<fieldset class="buttons">
									<g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
									&nbsp;<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
								</fieldset>
							</g:form>
						</td>

						<td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>
						<td><g:formatDate date="${todoInstance.date}" /></td>
						<td><g:formatDate date="${todoInstance.dateDone}" /></td>
						<td>${fieldValue(bean: todoInstance, field: "url")}</td>
						<td>${fieldValue(bean: todoInstance, field: "category")}</td>
						<td><todo:printIconFromBoolean value="${todoInstance.done}"/></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${todoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
