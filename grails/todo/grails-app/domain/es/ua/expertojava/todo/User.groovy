package es.ua.expertojava.todo

class User extends Person{
    String name
    String surnames
    String confirmPassword
    String email
    Date dateOfBirth
    String description
    String type

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false)
        surnames(blank:false)
        confirmPassword(blank:false, password:true)
        email(blank:false, email:true)
        dateOfBirth(nullable:true, validator: {
            if (it?.compareTo(new Date()) < 0)
                return true
            return false
        })
        description(maxSize:1000,nullable:true)
        type(blank: false, nullable: false, inList: ["administrador", "usuario"])
    }

    static transients = ["confirmPassword"]

    String toString(){
        "@${username}"
    }
}