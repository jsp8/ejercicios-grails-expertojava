package es.ua.expertojava.todo

class Tag {

    String name
    String color = "#FFFFFF"

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:true, unique:true)
        color (nullable: true, shared: "rgbcolor")
    }

    String toString(){
        name
    }
}

