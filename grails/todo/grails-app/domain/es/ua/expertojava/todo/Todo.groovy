package es.ua.expertojava.todo

class Todo {
    String title
    String description
    Date date
    Date reminderDate
    String url
    Category category
    Boolean done = false
    Date dateCreated
    Date lastUpdated
    Date dateDone
    User user

    static searchable = true

    static hasMany = [tags:Tag]
    static belongsTo = [Tag]

    static constraints = {
        title(blank: false)
        description(blank: true, nullable: true, maxSize: 1000)
        date(nullable: false)
        url(nullable: true, url: true)
        category(nullable: true)
        reminderDate(nullable: true,
                validator: { val, obj ->
                    if (val && obj.date) {
                        return val.before(obj?.date)
                    }
                    return true
                }
        )
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
        dateDone(nullable: true)
        user(nullable: true)
    }

    String toString(){
        "title"
    }
}
