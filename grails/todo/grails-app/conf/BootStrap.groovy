import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        try {

            def categoryHome = new Category(name:"Hogar").save()
            def categoryJob = new Category(name:"Trabajo").save()

            def tagEasy = new Tag(name:"Fácil", color: "#FFFFFF").save()
            def tagDifficult = new Tag(name:"Difícil", color: "#ABCDEF").save()
            def tagArt = new Tag(name:"Arte", color: "#123456").save()
            def tagRoutine = new Tag(name:"Rutina", color: "#123ABC").save()
            def tagKitchen = new Tag(name:"Cocina", color: "#ABC123").save()

            def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1)
            def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2)
            def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4)
            def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(), done: true)

            def rolAdmin = new Role(authority: "ROLE_ADMIN").save()
            def rolBasic = new Role(authority: "ROLE_BASIC").save()

            def usuarioAdmin = new User(username: "admin", password: "admin", name: "Admin", surnames: "ApellidoAdmin", confirmPassword: "admin", email: "admin@correo.es", type: "administrador").save()
            def usuario1Basic = new User(username: "usuario1", password: "usuario1", name: "Jorge", surnames: "Santamaría", confirmPassword: "usuario1", email: "usuario1@correo.es", type: "usuario").save()
            def usuario2Basic = new User(username: "usuario2", password: "usuario2", name: "Usuario2", surnames: "ApellidoUsuario2", confirmPassword: "usuario2", email: "usuario2@correo.es", type: "usuario").save()

            PersonRole.create usuarioAdmin, rolAdmin
            PersonRole.create usuario1Basic, rolBasic
            PersonRole.create usuario2Basic, rolBasic

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.user = usuario1Basic
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.user = usuario1Basic
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.user = usuario2Basic
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.user = usuario2Basic
            todoWriteUnitTests.save()

        } catch (Exception e) {
            println e
        }
    }

    def destroy = { }
}
