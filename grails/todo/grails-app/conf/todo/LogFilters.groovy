package todo

class LogFilters {

    def springSecurityService

    def filters = {
        all(controller:'category|tag|todo|user', action:'*') {

            before = {

            }

            after = { Map model ->

                def auth = springSecurityService.getPrincipal()
                String username = auth.username

                log.trace("- User ${username} - Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
                println ("FILTRO >>>> - User ${username} - Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
            }

            afterView = { Exception e ->

            }
        }
    }
}
