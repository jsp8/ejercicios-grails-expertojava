class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/api/todos"(resources:"todo")
        "/api/tags"(resources:"tag")
        "/api/categories"(resources:"category")
        "/api/users"(resources:"user")

        "/todos/$username"(controller:"todo",action:"showTodosByUser")

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
