package es.ua.expertojava.todo

import grails.transaction.Transactional

class CategoryService {

    def borrar(Category categoryInstance) {
        def todos = Todo.findAllByCategory(categoryInstance)

        todos.each { todo ->
            todo.category = null
        }
    }
}
