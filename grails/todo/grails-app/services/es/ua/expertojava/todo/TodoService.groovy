package es.ua.expertojava.todo

class TodoService {

    def springSecurityService

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }

    def borrar(Todo todoInstance) {
        def tags = todoInstance.tags.findAll()

        tags.each { tag ->
            todoInstance.removeFromTags(tag)
        }
    }

    def asignarUsuario(Todo todoInstance) {
        todoInstance.user = (User)springSecurityService.currentUser
    }

    def saveTodo(Todo todoInstance) {
        if (todoInstance.done == true)
            todoInstance.dateDone = new Date()
        else
            todoInstance.dateDone = null

        return todoInstance
    }

    def lastTodosDone(Integer hours, params) {
        Calendar cal = Calendar.getInstance()
        cal.setTime(new Date())
        Date hasta = cal.getTime()

        cal.set(Calendar.HOUR, cal.get(Calendar.HOUR)- hours)
        Date desde = cal.getTime()

        Todo.findAllByDateDoneBetween(desde, hasta, params)
    }

    def tareasUsuario() {
        def user = (User)springSecurityService.currentUser
        log.trace("Nombre de usuario consultado desde tareasUsuario: " + user.name)
        Todo.findAllByUser(user)
    }

    def showTodosByUser(params) {
        log.trace("Nombre de usuario consultado desde el servicio: " + params.username)
        def user = User.findByUsername(params.username)
        Todo.findAllByUser(user)
    }

    def buscarTareas(params) {

        def user = (User)springSecurityService.currentUser

        def todos = Todo.search([result:'every']) {
            must(queryString(params.q))
            must(term('$/Todo/user/id',User.findByName(user.name)?.id))
        }
    }
}
