package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Comprobar countNextTodos"() {
        given:
            def todoHoy = new Todo(title:"hoy", date: new Date() + 1)
            def todoManyana = new Todo(title:"mañana", date: new Date() + 2)
            def todoPasadoManyana = new Todo(title:"pasadoMañana", date: new Date() + 3)
            mockDomain(Todo,[todoHoy, todoManyana, todoPasadoManyana])
        when:
            def nextTodos = service.countNextTodos(2)
        then:
            Todo.count() == 3
            nextTodos == 2
    }

    void "Comprobar saveTodo con tarea no realizada"() {
        given:
            def todo = new Todo(title:"hoy", date: new Date(), done: false)
            mockDomain(Todo,[todo])
        when:
            def todoResultado = service.saveTodo(todo)
        then:
            todoResultado.dateDone == null
    }

    void "Comprobar saveTodo con tarea realizada"() {
        given:
            def todo = new Todo(title:"hoy", date: new Date(), done: true)
        when:
            mockDomain(Todo,[todo])
            def todoRealizado = service.saveTodo(todo)
        then:
            todoRealizado.dateDone == new Date()
    }
}
