package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "Si la fecha de recordatorio es anterior a la fecha de la tarea no dará problemas"() {
        given:
        def todo = new Todo(title:"Pintar cocina", date:new Date(), reminderDate: new Date() - 1)
        when:
        todo.validate()
        then:
        !todo?.errors["reminderDate"]
    }

    def "Si la fecha de recordatorio es posterior a la fecha de la tarea dará problemas"() {
        given:
        def todo = new Todo(title:"Pintar cocina", date:new Date(), reminderDate: new Date() + 1)
        when:
        todo.validate()
        then:
        todo?.errors["reminderDate"]
    }
}
