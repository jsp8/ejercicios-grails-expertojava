package es.ua.expertojava.todo

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.*
import spock.lang.*

@TestFor(TodoController)
@Mock(Todo)
class TodoControllerSpec extends Specification {

    def todoService = new TodoService()
    def springSecurityService = Mock(SpringSecurityService)
    def user

    def setup(){
        controller.todoService = todoService
        todoService.springSecurityService = springSecurityService
        user = new User(name:"Jorge", surnames:"Santamaria", username:"jsantamaria", password:"111111", confirmPassword: "111111", email:"jsantamaria@gmail.com", type:"usuario")
        springSecurityService.getCurrentUser() >> user
    }

    def populateValidParams(params) {
        assert params != null
        params["title"] = "Titulo tarea"
        params["date"] = new Date() + 2
        params["reminderDate"] = new Date() + 1
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.todoInstanceList
            model.todoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.todoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def todo = new Todo()
            todo.validate()
            controller.save(todo)

        then:"The create view is rendered again with the correct model"
            model.todoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params)

            controller.save(todo)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/api/todos/1'
            controller.flash.message != null
            Todo.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.show(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.edit(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/api/todos'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def todo = new Todo()
            todo.validate()
            controller.update(todo)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.todoInstance == todo

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params).save(flush: true)
            controller.update(todo)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/api/todos/$todo.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/api/todos'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def todo = new Todo(params).save(flush: true)

        then:"It exists"
            Todo.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(todo)

        then:"The instance is deleted"
            Todo.count() == 0
            response.redirectedUrl == '/api/todos'
            flash.message != null
    }

    void "Comprobar funcionamiento servicio listNextTodos"() {
        when:
            def todo1 = new Todo(title: "titulo1", date: new Date() + 1).save(flush: true)
            def todo2 = new Todo(title: "titulo2", date: new Date() + 20).save(flush: true)
            controller.listNextTodos(5)

        then:
            model.todoInstanceCount == 1
            view == 'index'
    }
}
